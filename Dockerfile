FROM alpine
RUN apk add py3-pip curl openssl bash jq
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.6/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin
RUN pip install awscli==1.18
RUN curl -L https://git.io/get_helm.sh | bash -s -- --version v3.8.2
